<?php

namespace App\Controller;

use App\DataAdapter\JsonDownloadAdapter;
use App\Service\Ad\AdService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class AdController extends AbstractController
{
    /**
     * @Route("/ads", name="ads")
     */
    public function index(AdService $adService)
    {
        /**
         * @var AdService $service
         */
        return $this->render('ads/index.html.twig', [
            'controller_name' => 'AdsController',
            'ads' => $adService->getAds()
        ]);
    }

    /**
     * @Route("/ads/download", name="adsDownload")
     */
    public function download(AdService $adService)
    {
        //FIXME: make this more flexible to allow more formats, only json required for now, so this will do
        $json = $adService->getAdsForDownload(new JsonDownloadAdapter());
        // Return a response with a specific content
        $response = new Response($json);
        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'ads.json'
        );
        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);
        // Dispatch request
        return $response;
    }

}
