<?php
/**
 * Created by PhpStorm.
 * User: cabezayunke
 * Date: 26/08/2018
 * Time: 11:25
 */
namespace App\DataSource\Http;

use App\DataSource\AdDataSource;

/**
 * Class AddsHttpDataSource
 *
 * In this case we fetch the ads data with a http request
 * Other sources could ne a DB, cache system, and so on
 */
class AdHttpDataSource implements AdDataSource
{
    public function getAds()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://feeds.spotahome.com/trovit-Ireland.xml');
        if($res->getStatusCode() === 200) {
            return $res->getBody()->getContents();
        }
        else {
            throw new \HttpException('[AdsHttpDataSource] Could not get Ads data');
        }
    }

}