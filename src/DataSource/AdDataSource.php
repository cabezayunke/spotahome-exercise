<?php
/**
 * Created by PhpStorm.
 * User: cabezayunke
 * Date: 26/08/2018
 * Time: 11:24
 */
namespace App\DataSource;

interface AdDataSource
{
    /**
     * Get ads data from different sources, format may vary
     * @return mixed
     */
    public function getAds();
}