<?php
/**
 * Created by PhpStorm.
 * User: cabezayunke
 * Date: 26/08/2018
 * Time: 11:13
 */
namespace App\Service\Ad;

use App\DataAdapter\DownloadAdapter;
use App\Model\Ad;
use App\DataSource\AdDataSource;
use App\DataAdapter\AdAdapter;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * Class AdsService
 * @package App\Service\Ads
 * Main service to use from the controller
 */
class AdService implements AdServiceInterface
{
    private $dataSource;
    private $dataAdapter;

    /**
     * AdsService constructor.
     * @param AdDataSource $dataSource
     * @param AdAdapter $dataAdapter
     */
    public function __construct(AdDataSource $dataSource, AdAdapter $dataAdapter)
    {
        $this->dataSource = $dataSource;
        $this->dataAdapter = $dataAdapter;
    }

    /**
     * Get ads as array of Ad objects
     *
     * @return Ad[]
     */
    public function getAds(): array
    {
        $data = $this->dataSource->getAds();
        if(!$data) {
            throw new NotFoundResourceException('Ads not found');
        }
        return $this->dataAdapter->parseAds($data);
    }

    /**
     * Get ads for download
     * We pass the adapter as param here because we could allow
     * multiple conversions from the same controller
     *
     * @param DownloadAdapter $adapter
     * @return array
     */
    public function getAdsForDownload(DownloadAdapter $adapter): string
    {
        $data = $this->dataSource->getAds();
        if(!$data) {
            throw new NotFoundResourceException('Ads not found');
        }
        $data = $this->dataAdapter->parseAds($data);
        return $adapter->export($data);
    }


}