<?php
/**
 * Created by PhpStorm.
 * User: cabezayunke
 * Date: 26/08/2018
 * Time: 11:13
 */
namespace App\Service\Ad;

use App\Model\Ad;
use App\DataAdapter\DownloadAdapter;


interface AdServiceInterface
{
    /**
     * Get ads array from some source
     *
     * @return Ad[]
     */
    public function getAds() : array;

    /**
     * Parses the ads data and converts it to the desire format for download
     * @return array
     */
    public function getAdsForDownload(DownloadAdapter $adapter) : string;
}