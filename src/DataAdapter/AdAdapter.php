<?php
/**
 * Created by PhpStorm.
 * User: cabezayunke
 * Date: 26/08/2018
 * Time: 11:31
 */

namespace App\DataAdapter;

use App\Model\Ad;


interface AdAdapter
{
    /**
     * Return array of Ads from original data
     * @param $data
     * @return mixed
     */
    public function parseAds($data) : array;

    /**
     * Return ad object from the original data
     *
     * @param $data
     * @return Ad
     */
    public function buildAdObject($data) : Ad;
}