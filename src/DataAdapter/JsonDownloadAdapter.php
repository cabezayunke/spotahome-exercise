<?php
/**
 * Created by PhpStorm.
 * User: cabezayunke
 * Date: 26/08/2018
 * Time: 12:52
 */

namespace App\DataAdapter;

use App\Model\Ad;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class JsonDownloadAdapter implements DownloadAdapter
{
    public function export($data)
    {
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        return $serializer->serialize($data, 'json');
    }

}