<?php
/**
 * Created by PhpStorm.
 * User: cabezayunke
 * Date: 26/08/2018
 * Time: 11:30
 */

namespace App\DataAdapter;

use App\Model\Ad;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class XmlAdAdapter
 *
 * Build an Ad object from XML data
 * We could have one adapter for other formats (JSON, etc)
 *
 * @package App\Service\Ad
 */
class XmlAdAdapter implements AdAdapter
{
    public function parseAds($data): array
    {
        $xmlEncoder = new XmlEncoder();
        $xmlEncoder->setRootNodeName('trovit');
        $xml = $xmlEncoder->decode($data, 'xml');
        //dd($xml["ad"]);
        return array_map(function ($ad) {
            return $this->buildAdObject($ad);
        },
        $xml["ad"]);
    }

    public function buildAdObject($data): Ad
    {
        //dd($data);
        // FIXME: couldn't find a way to transform this, so doing it manually for now
        $ad = new Ad();
        $ad->setId($data["id"]);
        $ad->setCity($data["city"]);
        $ad->setLink($data["url"]);
        $ad->setTitle($data["title"]);
        if($data["pictures"] && $data["pictures"]["picture"]) {
            $ad->setImage($data["pictures"]["picture"][0]["picture_url"]);
        }
        return $ad;
    }

}