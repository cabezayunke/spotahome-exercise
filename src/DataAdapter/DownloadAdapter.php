<?php
/**
 * Created by PhpStorm.
 * User: cabezayunke
 * Date: 26/08/2018
 * Time: 12:51
 */

namespace App\DataAdapter;


interface DownloadAdapter
{
    /**
     * Export data in the desired format for download
     * @param $data
     * @return mixed
     */
    public function export($data);
}