# Spotahome exercise

Tasks:
1. Fetch the remote XML file http://feeds.spotahome.com/trovit-Ireland.xml in your
backend code
2. Show a list of data: id, title, link, city and main image from the XML downloaded
3. It should be able to sort by any field by clicking on the table header
4. Allow to download the data sorted as shown in screen as a JSON file

### Done: 1, 2 and 4
### TODO: 3

